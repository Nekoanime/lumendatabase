import splinter
import sys
import requests


def main(domain='nekoanimedd.com'):
    sp  = splinter.Browser("chrome", headless=True)
    sp.visit("https://lumendatabase.org/notices/search?sort_by=&term={}&utf8=%E2%9C%93".format(domain))
    sp.is_element_present_by_xpath("//li[@class='notice result']", 5)
    el = sp.find_by_xpath("//span[@class='page']")
    old_url = None
    urls = []
    i = 1
    while True:
        print >> sys.stderr, "loaded"
        results = sp.find_by_xpath("//li[@class='notice result']/h3/a")
        for r in results:
            urls.append(r["href"])
        res = sp.find_by_xpath("//a[@rel='next']")
        if res:
            u = res.pop()["href"]
            old_url = u
            print >> sys.stderr, "visiting {}".format(u)
            sp.visit(u)
        else:
            i = i+1
            if i >= len(el):
                break
            print >> sys.stderr, "cicle {}".format(old_url)
            sp.visit(old_url)


    for u in urls:
        print(u)

    for u in urls:
        print >> sys.stderr, "visitin notice {}".format(u)
        sp.visit(u)
        sp.is_element_present_by_text("Close", 20)
        s = sp.find_by_text("Close")
        if s.visible:
            s.click()
        datas = sp.find_by_xpath("//li[contains(text(), '{}')]".format(domain))
        for d in datas:
            if domain in d.text:
                r = requests.get(d.text)
                print("{} {}".format(r.status_code, d.text))

if __name__ == "__main__":
    domain = sys.argv[1]
    if domain:
        main(domain)
    else:
        main()
